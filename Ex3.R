setwd("C:/Users/nir09/Desktop/���/�������/��� �/����� �/����� ��� ������ �����/������ ���/����� ��������")
titanic.df<- read.csv('titanic.csv')

##Question 1##
naColumns<- function(dataFrame){
  apply(titanic.df,2,function(dataFrame)any(is.na.data.frame(dataFrame)))
}

naColumns(titanic.df)


booleanVec<-function(dataFrame){
  return(as.vector(naColumns(dataFrame)))
}

booleanVec(titanic.df)


##Question 2##
replaceNa <-function(vec)
{
  med<-median(vec ,na.rm=TRUE)
  if (any(is.na(vec)==TRUE) & (is.numeric(vec)))
  {
    changeVec<-replace(vec,is.na(vec),med)
  }
  return(changeVec)
}

ageMed<-replaceNa(titanic.df$Age)

##Question 3##
titanicSurvivels<- factor(titanic.df$Survived, levels=c(0,1),  labels = c("Died","Survived"))

titanicPclass<- factor(titanic.df$Pclass, levels=c(1,2,3),  labels = c("Upper","Middle","Lower"))
