install.packages('ggplot2')
library(ggplot2)

install.packages("ggplot2movies")
library(ggplot2movies)

moviesEx<- movies

str(moviesEx)

##Question1##
ggplot(moviesEx,aes(rating)) + geom_histogram(bins=20)
## ���� ����� �� ������� ������� �������, ��� ������ ������� ���� ������ ##

##Question2##
ggplot(moviesEx,aes(year,rating)) + stat_smooth(method=lm)
## ���� ������ �� ����� ������ ���� ����� ����� ��� �� ���� �����##

##Question3##
ggplot(moviesEx,aes(year,budget)) + geom_col()
## ���� ����� ���� ������ ������ ������ ������ ����� ����� ������� ##

##Question4##
ggplot(moviesEx,aes(length,budget)) + geom_point()+ xlim(0,300)  + stat_smooth(method=lm)
## ���� ����� �� �������� ������� ����� ������� ������ ����� ���� 100 ���� �� ����� ���� �����
##
##Question5##
ggplot(moviesEx,aes(length,rating)) + geom_col()

##Question6##
ggplot(moviesEx,aes(rating,budget))+geom_point()  + stat_smooth(method=lm)
##��� ���� �� ���� ����� ��� ���� ��� ����� ������ ���� ����� ����##

##Question7##
aniMovies<- moviesEx$Animation
ggplot(moviesEx,aes(aniMovies==1,rating))+geom_boxplot()

##Question8##
actionMovies<- moviesEx$Action
ggplot(moviesEx,aes(actionMovies==1,rating))+geom_boxplot()
